function OnPremiseTransport(options) {
    Sentry.Transports.XHRTransport.call(this, options);
    this.url = window.apiUrl + '/api/monitoring/track-js-error?component=' + window.sentryComponent;
}

OnPremiseTransport.prototype = Object.create(Sentry.Transports.XHRTransport.prototype);

Sentry.init({
    dsn: 'https://dummy@dummy/0',
    transport: OnPremiseTransport,
});
