<?php

namespace Apeisia\MonitoringBundle\Twig;

use Apeisia\MonitoringBundle\Service\DSNService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ApeisiaMonitoringTwigExtension extends AbstractExtension
{
    /**
     * @var DSNService
     */
    private $DSNService;

    public function __construct(DSNService $DSNService)
    {
        $this->DSNService = $DSNService;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('monitoringCode', [$this, 'monitoringCode'], ['is_safe' => ['html']]),
        ];
    }

    public function monitoringCode($id, $component = 'main')
    {
        if ($id !== 'sentry') {
            throw new \InvalidArgumentException('monitoringCode: only sentry is allowed as of now.');
        }

        if (!$this->DSNService->isComponentDefined($component)) {
            return '';
        }

        if (!array_key_exists('API_URL', $_ENV)) {
            $apiUrl = '';
        } else {
            $apiUrl = $_ENV['API_URL'];
        }

        return
            '<script src="' . $apiUrl . '/bundles/apeisiamonitoring/bundle.min.js"></script>' .
            '<script>window.sentryComponent = "' . $component . '";' .
            'window.apiUrl = "' . $apiUrl . '";' .
            '</script>' .
            '<script src="' . $apiUrl . '/bundles/apeisiamonitoring/onpremise.js"></script>';
    }
}
