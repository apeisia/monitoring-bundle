1. Enable bundle.
2. Add routing:
    ```yaml
    apeisia_monitoring:
        resource: "@ApeisiaMonitoringBundle/Controller/"
        type:     rest
    ```
3. Set DSN in `parameters.yaml`:
    ```yaml
   apeisia.monitoring.sentry_dsn: <dsn>
   ```
   OR if there is more than one dsn:
   ```yaml
   apeisia.monitoring.sentry_dsn:
       <component>: <dsn>
       ...  
    ```

4. Add tracking code to template:
    ```twig
       {{ monitoringCode('sentry', '<component>'}}
    ```
