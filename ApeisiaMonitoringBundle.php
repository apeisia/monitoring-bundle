<?php

namespace Apeisia\MonitoringBundle;

use Apeisia\MonitoringBundle\DependencyInjection\ApeisiaMonitoringBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApeisiaMonitoringBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new ApeisiaMonitoringBundleExtension();
    }

}
