<?php

namespace Apeisia\MonitoringBundle\Controller;

use Apeisia\MonitoringBundle\Service\DSNService;
use FOS\RestBundle\Controller\Annotations as Rest;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JavascriptController extends AbstractController
{
    /**
     * @Rest\Post("/api/monitoring/track-js-error")
     * @param Request $request
     * @param DSNService $dsnService
     * @return Response|null
     */
    public function trackErrorAction(Request $request, DSNService $dsnService)
    {
        $url  = $dsnService->getRequestUrlForComponent($request->get('component'));
        $data = $request->getContent();

        $client = new Client();
        $response = $client->post($url, [
            'body' => $data,
            'http_errors' => false,
        ]);

        return new Response($response->getBody()->getContents(), $response->getStatusCode());
    }
}
