<?php

namespace Apeisia\MonitoringBundle\Service;

class DSNService
{

    private $dsnMap;

    /**
     * DSNService constructor.
     * @param array|string $dsnMap
     */
    public function __construct($dsnMap)
    {
        $this->dsnMap = $dsnMap;
    }

    public function isComponentDefined($component)
    {
        if (!$this->dsnMap) {
            return false;
        }

        if (is_array($this->dsnMap) && !array_key_exists($component, $this->dsnMap)) {
            return false;
        }

        return true;
    }

    public function getDSNForComponent($component)
    {
        if (!is_array($this->dsnMap)) {
            return $this->dsnMap;
        }

        if (!array_key_exists($component, $this->dsnMap)) {
            throw new \InvalidArgumentException('Component ' . $component . ' is not defined.');
        }

        return $this->dsnMap[$component];
    }

    public function getRequestUrlFromDSN($dsn)
    {
        // regex taken from setnry javascript library
        if (!preg_match('/^(?:(\w+):)\/\/(?:(\w+)(?::(\w+))?@)([\w\.-]+)(?::(\d+))?\/(.+)/', $dsn, $matches)) {
            throw new \RuntimeException('Invalid dsn format.');
        }

        $protocol   = $matches[1];
        $user       = $matches[2];
        $host       = $matches[4];
        $projectId  = $matches[6];
        $apiVersion = 7; // replace this when updating the javascript sdk

        return $protocol . '://' . $host . '/api/' . $projectId . '/store/?sentry_key=' . $user . '&sentry_version=' . $apiVersion;
    }

    public function getRequestUrlForComponent($component)
    {
        return $this->getRequestUrlFromDSN($this->getDSNForComponent($component));
    }
}
